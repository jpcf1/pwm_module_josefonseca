
import pwm_module_pkg::*;

    timeunit      1ns;
    timeprecision 1fs;

class PwmGenDriver;


    // Mailbox handler to be assigned in new()
    mailbox #(PwmCoreSeqItem)  seqncer_to_driver;
    mailbox #(PwmCoreSeqItem) drv_to_scb;
    
    event drv_finished;

    // Interface handle to toggle signals
    virtual pwm_gen_if itf;

    function new(virtual pwm_gen_if itf, 
                input mailbox #(PwmCoreSeqItem) seqncer_to_driver,
                input mailbox #(PwmCoreSeqItem) drv_to_scb,
                input event drv_finished);
        this.seqncer_to_driver = seqncer_to_driver;
        this.drv_to_scb        = drv_to_scb; 
        this.itf               = itf;
        this.drv_finished      = drv_finished;
    endfunction

    virtual task run();
        // SequenceItem handle
        PwmCoreSeqItem   seq_item;

        @(negedge itf.core_rst_n);
        seqncer_to_driver.get(seq_item);
        $display("Applying new Seq Item: %h %h %h", seq_item.freq, seq_item.dutyc, seq_item.res);
        itf.freq_sel  <= seq_item.freq;
        itf.dutyc_sel <= seq_item.dutyc;
        itf.res_sel   <= seq_item.res;
        
        // Send Tx to Scoreboard
        drv_to_scb.put(seq_item.copy());
        
        // After reset, wait for the first edge
        @(negedge itf.pwm_out);

        while(seqncer_to_driver.num()) begin
            if(itf.pol_sel == ACTIVE_HIGH) begin
                @(negedge itf.pwm_out);
            end
            else begin
                @(posedge itf.pwm_out);
            end

            seqncer_to_driver.get(seq_item);

            @(posedge itf.core_clk);
            $display("Applying new Seq Item: %h %h %h", seq_item.freq, seq_item.dutyc, seq_item.res);
            itf.freq_sel  <= seq_item.freq;
            itf.dutyc_sel <= seq_item.dutyc;
            itf.res_sel   <= seq_item.res;

            // Send Tx to Scoreboard
            drv_to_scb.put(seq_item.copy());
        end
        -> this.drv_finished;
    endtask
endclass

class APBSlaveDriver;


    // Mailbox handler to be assigned in new()
    mailbox #(PwmCoreSeqItem) seqncer_to_driver;
    mailbox #(PwmCoreSeqItem) drv_to_scb;
    
    event drv_finished;
    event drv_tx_done;

    int right_moment_to_apply_tx=0;
    logic [1:0] rd_data;

    // Interface handle to toggle signals
    virtual apb_if itf;

    function new(virtual apb_if itf, 
                input mailbox #(PwmCoreSeqItem) seqncer_to_driver,
                input mailbox #(PwmCoreSeqItem) drv_to_scb,
                input event drv_tx_done,
                input event drv_finished);
        this.seqncer_to_driver = seqncer_to_driver;
        this.drv_to_scb        = drv_to_scb; 
        this.itf               = itf;
        this.drv_tx_done       = drv_tx_done;
        this.drv_finished      = drv_finished;
    endfunction

    virtual task run();
        // SequenceItem handle
        PwmCoreSeqItem   seq_item;
        $timeformat(-9, 6, "ns", 12);

        @(posedge itf.preset_n);
        seqncer_to_driver.get(seq_item);

        // Setup Phase 
        @(posedge itf.pclk);
        itf.psel      <= 1'b1;
        itf.penable   <= 1'b0;
        itf.pwrite    <= 1'b1;
        itf.pstrb     <= 2'b01;
        itf.pwdata    <= seq_item.dutyc[7:0];
        itf.paddr     <= PWM_CFG1_DUTYC_LO_REG;

        // Data Phase 
        @(posedge itf.pclk);
        itf.psel      <= 1'b1;
        itf.penable   <= 1'b1;
        itf.pwrite    <= 1'b1;

        // Setup Phase 
        @(posedge itf.pclk);
        itf.psel      <= 1'b1;
        itf.penable   <= 1'b0;
        itf.pwrite    <= 1'b1;
        itf.pstrb     <= 2'b01;
        itf.pwdata    <= seq_item.dutyc[15:8];
        itf.paddr     <= PWM_CFG1_DUTYC_HI_REG;

        // Data Phase 
        @(posedge itf.pclk);
        itf.psel      <= 1'b1;
        itf.penable   <= 1'b1;
        itf.pwrite    <= 1'b1;

        // Setup Phase 
        @(posedge itf.pclk);
        itf.psel      <= 1'b1;
        itf.penable   <= 1'b0;
        itf.pwrite    <= 1'b1;
        itf.pwdata    <= {1'b1, ACTIVE_HIGH, seq_item.res, seq_item.freq};
        itf.paddr     <= PWM_CFG2_RES_FREQ_REG;
        
        // Data Phase 
        @(posedge itf.pclk);
        itf.psel      <= 1'b1;
        itf.penable   <= 1'b1;
        itf.pwrite    <= 1'b1;

        // Setup Phase 
        @(posedge itf.pclk);
        itf.psel      <= 1'b1;
        itf.penable   <= 1'b0;
        itf.pwrite    <= 1'b1;
        itf.pwdata    <= {7'd0, 1'b1};
        itf.paddr     <= PWM_CFG2_CTRL_REG;
        
        // Data Phase 
        @(posedge itf.pclk);
        itf.psel      <= 1'b1;
        itf.penable   <= 1'b1;
        itf.pwrite    <= 1'b1;
        $display("Driver: [%t] Applying new Seq Item: %h %h %h", $realtime, seq_item.freq, seq_item.dutyc, seq_item.res);
        // Send Tx to Scoreboard
        drv_to_scb.put(seq_item.copy());
        ->drv_tx_done;

        @(posedge itf.pclk);
        itf.psel      <= 1'b0;
        itf.penable   <= 1'b0;
        itf.pwrite    <= 1'b0;

        // Simulated Processor delay
        repeat ($urandom_range(1,10))
            @(posedge itf.pclk);

        while(seqncer_to_driver.num()) begin


            while(1) begin
                // Setup Phase 
                @(posedge itf.pclk);
                itf.psel      <= 1'b1;
                itf.penable   <= 1'b0;
                itf.pwrite    <= 1'b0;
                itf.pstrb     <= 2'b00;
                itf.paddr     <= PWM_STATUS_REG;

                // Data Phase 
                @(posedge itf.pclk);
                itf.psel     <= 1'b1;
                itf.penable  <= 1'b1;
                itf.pwrite   <= 1'b0;
                itf.pstrb    <= 2'b00;
                rd_data      <= itf.prdata;
                
                // when the signal is at the same level as the selected
                // polarity, we are at the very start of the current cycle
                // so, we decide to apply the tx here. Also, check that configuration
                // has been applied
                if(rd_data[0] == rd_data[1] && rd_data[1] == ACTIVE_HIGH)
                    break;
            end
            $display("Driver: [%t] Left First Loop", $realtime);
            
            // Simulated Processor processing delay
            repeat ($urandom_range(1,10))
                @(posedge itf.pclk);

            // Apply new Tx
            seqncer_to_driver.get(seq_item);

            // Setup Phase 
            @(posedge itf.pclk);
            itf.psel      <= 1'b1;
            itf.penable   <= 1'b0;
            itf.pwrite    <= 1'b1;
            itf.pstrb     <= 2'b01;
            itf.pwdata    <= seq_item.dutyc[7:0];
            itf.paddr     <= PWM_CFG1_DUTYC_LO_REG;

            // Data Phase 
            @(posedge itf.pclk);
            itf.psel      <= 1'b1;
            itf.penable   <= 1'b1;
            itf.pwrite    <= 1'b1;

            // Setup Phase 
            @(posedge itf.pclk);
            itf.psel      <= 1'b1;
            itf.penable   <= 1'b0;
            itf.pwrite    <= 1'b1;
            itf.pstrb     <= 2'b01;
            itf.pwdata    <= seq_item.dutyc[15:8];
            itf.paddr     <= PWM_CFG1_DUTYC_HI_REG;

            // Data Phase 
            @(posedge itf.pclk);
            itf.psel      <= 1'b1;
            itf.penable   <= 1'b1;
            itf.pwrite    <= 1'b1;

            // Setup Phase 
            @(posedge itf.pclk);
            itf.psel      <= 1'b1;
            itf.penable   <= 1'b0;
            itf.pwrite    <= 1'b1;
            itf.pwdata    <= {1'b1, ACTIVE_HIGH, seq_item.res, seq_item.freq};
            itf.paddr     <= PWM_CFG2_RES_FREQ_REG;
            
            // Data Phase 
            @(posedge itf.pclk);
            itf.psel      <= 1'b1;
            itf.penable   <= 1'b1;
            itf.pwrite    <= 1'b1;
            $display("Driver: [%t] Applying new Seq Item: %h %h %h", $realtime, seq_item.freq, seq_item.dutyc, seq_item.res);
            // Send Tx to Scoreboard
            drv_to_scb.put(seq_item.copy());
            ->drv_tx_done;

            @(posedge itf.pclk);
            itf.psel      <= 1'b0;
            itf.penable   <= 1'b0;
            itf.pwrite    <= 1'b0;

            while(1) begin
                // Setup Phase 
                @(posedge itf.pclk);
                itf.psel      <= 1'b1;
                itf.penable   <= 1'b0;
                itf.pwrite    <= 1'b0;
                itf.pstrb     <= 2'b00;
                itf.paddr     <= PWM_STATUS_REG;

                // Data Phase 
                @(posedge itf.pclk);
                itf.psel     <= 1'b1;
                itf.penable  <= 1'b1;
                itf.pwrite   <= 1'b0;
                itf.pstrb     <= 2'b00;
                rd_data      <= itf.prdata;

                // when the signal is at the same level as the selected
                // polarity, we are at the very start of the current cycle
                // so, we decide to apply the tx here
                if(rd_data[0] != rd_data[1])
                    break;
            end
            $display("Driver: [%t] Left Second Loop", $realtime);


        end
        -> this.drv_finished;
    endtask
endclass
