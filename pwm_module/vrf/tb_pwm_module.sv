
import pwm_module_pkg::*;


`include "interfaces.sv"
`include "seqitem.sv"
`include "drivers.sv"
`include "monitors.sv"
`include "sequencers.sv"
`include "scoreboards.sv"


module tb_pwm_module();

    timeunit      1ns;
    timeprecision 1fs;

    // Clock and reset
    bit core_clk;
    bit pclk;
    
    // Module Interface
    pwm_gen_if pwm_gen_if1(core_clk);
    apb_if     apb_if1(pclk);

    // Params
    localparam          T_APB           = 10ns;    // 100MHz
    //localparam          T_APB           = 1.907349ns;
    localparam          TCORE           = 1.907349ns;
    localparam          TSWITCH         = 1/2000.0;
    localparam          NUM_TESTS       = `NUM_TESTS_ARGV;
    localparam realtime SIM_TIMEOUT     = NUM_TESTS*(2ms);
    localparam real     ERROR_TOLERANCE = 0.00005;

    // Module Instantiation
    pwm_module #(
        .ASYNC(1'b1),
        .APB_SLV_DW(16),
        .APB_SLV_AW(32)
    ) pwm_module_inst (
        // APB Slave Side
        .pclk(pclk),
        .preset_n(apb_if1.preset_n),
        .paddr(apb_if1.paddr),
        .pwrite(apb_if1.pwrite),
        .psel(apb_if1.psel),
        .penable(apb_if1.penable),
        .pwdata(apb_if1.pwdata),
        .pstrb(apb_if1.pstrb),
        .prdata(apb_if1.prdata),
        .pready(apb_if1.pready),
        .pslverr(apb_if1.pslverr),

        // PWM Core Side
        .core_clk(core_clk),
        .core_rst_n(pwm_gen_if1.core_rst_n),
        .pwm_o(pwm_gen_if1.pwm_out)
    );

    // Clock Generation
    always begin
        #(TCORE/2.0) core_clk = ~core_clk; 
    end

    always begin
        #(T_APB/2.0) pclk = ~pclk; 
    end
    // Testbench Block Instantiation 
    mailbox #(PwmCoreSeqItem)   mb_seqncer_to_driver;
    mailbox #(PwmCoreSeqItem)   mb_drv_to_scb;
    mailbox #(PwmCoreMonitorTx) mb_mon_to_scb;

    event drv_finish;
    event mon_finish;
    event drv_tx_done;

    PwmGenTestSequencer sequencer;
    APBSlaveDriver      driver;
    PwmGenMonitor       monitor;
    PwmCoreScoreboard   scoreboard;
    
    initial begin
        // Initialization Assignments
        mb_seqncer_to_driver = new();
        mb_mon_to_scb        = new();
        mb_drv_to_scb        = new();
        driver               = new(apb_if1, mb_seqncer_to_driver, mb_drv_to_scb, drv_tx_done, drv_finish);
        monitor              = new(pwm_gen_if1, mb_mon_to_scb, NUM_TESTS, drv_tx_done, mon_finish);
        sequencer            = new(mb_seqncer_to_driver);
        scoreboard           = new(mb_drv_to_scb, mb_mon_to_scb, NUM_TESTS, 0.005);
        
        sequencer.generate_tests(NUM_TESTS);
        
        // Initialize Regs
        core_clk               = 0;

        // Start the execution threads of Drivers and Monitors
        fork 
            driver.run();
            monitor.run();
            begin 
                #(SIM_TIMEOUT);
                $display("ERROR: Simulation Timeout");
                $finish(-1);
            end
        join_none

        // Active Low Reset, with sync removal
        fork
            begin
                pwm_gen_if1.core_rst_n = 1;
                #(2*TCORE) 
                pwm_gen_if1.core_rst_n = 0;
                #(2*TCORE) 
                @(posedge core_clk);  // Synchonized removal
                pwm_gen_if1.core_rst_n = 1;
            end
            begin
                apb_if1.preset_n = 1;
                #(2*T_APB) 
                apb_if1.preset_n = 0;
                #(2*T_APB) 
                @(posedge pclk); // Synchonized removal
                apb_if1.preset_n = 1;
            end
        join

    
        // Wait for the Driver to finish sending Txsa, and then wait a bit more
        @drv_finish;
        wait(mon_finish.triggered);

        // Calculate errors and evaluate simulation run
        scoreboard.wrap_up();
        $finish(scoreboard.print_final_report());

    end
        
endmodule

    
