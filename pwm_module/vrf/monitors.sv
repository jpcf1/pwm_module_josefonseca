
class PwmGenMonitor;
    // Mailbox handler to be assigned in new()
    mailbox #(PwmCoreMonitorTx) mon_to_scb;
    event mon_finish;
    event drv_tx_done;
    int num_exp_tx;
    int num_tx=0;
    bit first_period=1;

    // Interface handle to toggle signals
    virtual pwm_gen_if itf;

    function new(virtual pwm_gen_if itf, 
                 input mailbox #(PwmCoreMonitorTx) mon_to_scb, 
                 int num_exp_tx, 
                 event drv_tx_done, 
                 event mon_finish);
        this.mon_to_scb  = mon_to_scb;
        this.itf         = itf;
        this.num_exp_tx  = num_exp_tx;
        this.mon_finish  = mon_finish;
        this.drv_tx_done = drv_tx_done;
    endfunction

    virtual task run();
        realtime t1;
        realtime t2;
        PwmCoreMonitorTx mon_tx = new();
        $timeformat(-9, 6, "ns", 12);

        // Wait for reset deassertion
        @(posedge itf.core_rst_n)
        wait(drv_tx_done.triggered);

        while(1) begin    
            @(posedge itf.pwm_out);
            if(!first_period) begin
                mon_tx.period = $realtime - t1;

                $display("Monitor: [%t] Received new Tx: Period=%t DutyC=%t", $realtime, mon_tx.period, mon_tx.duty_time);

                mon_to_scb.put(mon_tx.copy());
                this.num_tx++;
                if(this.num_tx == this.num_exp_tx) begin
                    // Signal that the Monitor has finished its job
                    -> this.mon_finish;
                    break;
                end
            end
            t1 = $realtime;
            $display("Monitor: [%t] Rising Edge", t1);
            first_period = 0;

            @(negedge itf.pwm_out);
            mon_tx.duty_time = $realtime - t1;
            $display("Monitor: [%t] Falling Edge", $realtime);

        end

    endtask
endclass
