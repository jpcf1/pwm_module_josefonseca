
import pwm_module_pkg::*;


`include "interfaces.sv"
`include "seqitem.sv"
`include "drivers.sv"
`include "monitors.sv"
`include "sequencers.sv"
`include "scoreboards.sv"


module tb_pwm_core();

    timeunit      1ns;
    timeprecision 1fs;

    // Clock and reset
    bit core_clk;
    
    // Module Interface
    pwm_gen_if pwm_gen_if1(core_clk);

    // Params
    localparam          TCORE           = 1.907349ns;
    localparam          TSWITCH         = 1/2000.0;
    localparam          NUM_TESTS       = `NUM_TESTS_ARGV;
    localparam realtime SIM_TIMEOUT     = NUM_TESTS*(2ms);
    localparam real     ERROR_TOLERANCE = 0.00005;

    // Module Instantiation
    `ifdef ARCH_COUNTER
        pwm_core_counter_arch   #(
    `elsif ARCH_DELAYLINE
        pwm_core_delayline_arch #(
    `endif
        .ASYNC(1'b1)
    ) pwm_core_inst (
        .core_clk(core_clk),
        .core_rst_n(pwm_gen_if1.core_rst_n),
        .freq_sel(pwm_gen_if1.freq_sel),
        .res_sel(pwm_gen_if1.res_sel),
        .dutyc_sel(pwm_gen_if1.dutyc_sel),
        .polarity(pwm_gen_if1.pol_sel),
        .enable(pwm_gen_if1.en),
        .pwm_out(pwm_gen_if1.pwm_out)
    );

    // Clock Generation
    always begin
        #(TCORE/2.0) core_clk = ~core_clk; 
    end

    // Testbench Block Instantiation 
    mailbox #(PwmCoreSeqItem)   mb_seqncer_to_driver;
    mailbox #(PwmCoreSeqItem)   mb_drv_to_scb;
    mailbox #(PwmCoreMonitorTx) mb_mon_to_scb;

    event drv_finish;
    event mon_finish;

    PwmGenTestSequencer sequencer;
    PwmGenDriver        driver;
    PwmGenMonitor       monitor;
    PwmCoreScoreboard   scoreboard;
    
    initial begin
        // Initialization Assignments
        mb_seqncer_to_driver = new();
        mb_mon_to_scb        = new();
        mb_drv_to_scb        = new();
        driver               = new(pwm_gen_if1, mb_seqncer_to_driver, mb_drv_to_scb, drv_finish);
        monitor              = new(pwm_gen_if1, mb_mon_to_scb, NUM_TESTS, mon_finish);
        sequencer            = new(mb_seqncer_to_driver);
        scoreboard           = new(mb_drv_to_scb, mb_mon_to_scb, NUM_TESTS, 0.005);
        
        sequencer.generate_tests(NUM_TESTS);
        
        // Initialize Regs
        pwm_gen_if1.en         = 1;
        pwm_gen_if1.pol_sel    = ACTIVE_HIGH;
        core_clk               = 0;

        // Start the execution threads of Drivers and Monitors
        fork 
            driver.run();
            monitor.run();
            begin 
                #(SIM_TIMEOUT);
                $display("ERROR: Simulation Timeout");
                $finish(-1);
            end
        join_none

        // Active Low Reset, with sync removal
        pwm_gen_if1.core_rst_n = 1;
        #(2*TCORE) 
        pwm_gen_if1.core_rst_n = 0;
        #(2*TCORE) 
        @(posedge core_clk);
        pwm_gen_if1.core_rst_n = 1;

    
        // Wait for the Driver to finish sending Txsa, and then wait a bit more
        @drv_finish;
        wait(mon_finish.triggered);

        // Calculate errors and evaluate simulation run
        scoreboard.wrap_up();
        $finish(scoreboard.print_final_report());

    end
        
endmodule

    
