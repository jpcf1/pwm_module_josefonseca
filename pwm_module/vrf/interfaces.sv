interface pwm_gen_if(input bit core_clk);
    bit        core_rst_n;
    bit  [2:0] freq_sel;
    bit  [2:0] res_sel;    
    bit [15:0] dutyc_sel;
    bit        pol_sel;
    bit        en;
    logic        pwm_out;
endinterface


interface apb_if 
    #(
      parameter APB_SLV_AW=32,
      parameter APB_SLV_DW=16
     )
    (
        input bit pclk
    );
    bit                    preset_n;
    bit   [APB_SLV_AW-1:0] paddr;
    bit                    pwrite;
    bit                    psel;
    bit                    penable;
    bit   [APB_SLV_DW-1:0] pwdata;
    bit [APB_SLV_DW/8-1:0] pstrb;
    logic [APB_SLV_DW-1:0] prdata;
    logic                  pready;
    logic                  pslverr;
endinterface
