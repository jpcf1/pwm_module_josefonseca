
import pwm_module_pkg::*;

timeunit      1ns;
timeprecision 1fs;

class PwmCoreSeqItem;
    

    rand period_divider_t freq;
    rand     resolution_t res;
    rand     logic [15:0] dutyc;
    rand       polarity_t pol;
    realtime              exp_period;
    realtime              exp_duty_time;

    function void post_randomize();
        // Pwm Generator Model
        exp_period    = pwm_module_pkg::freq_to_time(this.freq);
        exp_duty_time = pwm_module_pkg::dutyc_to_time(this.dutyc, this.freq, this.res);
    endfunction

    function PwmCoreSeqItem copy();
        PwmCoreSeqItem copy_of_this = new();
        copy_of_this.freq           =  freq;            
        copy_of_this.res            =  res;
        copy_of_this.dutyc          =  dutyc;
        copy_of_this.pol            =  pol;
        copy_of_this.exp_period     =  exp_period;
        copy_of_this.exp_duty_time  =  exp_duty_time;
        return copy_of_this;
    endfunction

    constraint dutyc_word_match_resolution {
        // When the resolution chosen is smaller
        // than the register size, we must restrict
        // the duty cycle word accordingly
        dutyc < 2**(16-res);
    }

    constraint max_resolution_only {
        res == R_16;
    }

    constraint max_freq_only {
        freq == F_2000;
    }
    
endclass

class PwmCoreMonitorTx;
    realtime period;
    realtime duty_time;

    function PwmCoreMonitorTx copy();
        PwmCoreMonitorTx copy_of_this = new();
        copy_of_this.period    = this.period;
        copy_of_this.duty_time = this.duty_time;
        return copy_of_this;
    endfunction
endclass
