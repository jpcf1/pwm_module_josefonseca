
import pwm_module_pkg::*;

class PwmGenTestSequencer;

    // Mailbox handler to be assigned in new()
    mailbox #(PwmCoreSeqItem) seqncer_to_driver;

    function new(input mailbox #(PwmCoreSeqItem) seqncer_to_driver);
        this.seqncer_to_driver = seqncer_to_driver;
    endfunction

    task generate_tests(int NUM_TESTS);
        PwmCoreSeqItem seq_item = new();
        repeat(NUM_TESTS) begin
            seq_item.randomize();
            seq_item.max_resolution_only.constraint_mode(0);
            seq_item.max_freq_only.constraint_mode(0);
            this.seqncer_to_driver.put(seq_item.copy());
        end
    endtask

endclass
