
import pwm_module_pkg::*;

class PwmCoreScoreboard;

    mailbox #(PwmCoreSeqItem)   drv_to_scb;
    mailbox #(PwmCoreMonitorTx) mon_to_scb;
    PwmCoreSeqItem   drv_tx[];
    PwmCoreMonitorTx mon_tx[];

    integer num_tx;
    integer num_tests;
    real    tolerance;
    real    error_per[];
    real    error_dutyc[];
    real    num_error_per=0;
    real    num_error_dutyc=0;

    function new(input mailbox #(PwmCoreSeqItem)   drv_to_scb,
                 input mailbox #(PwmCoreMonitorTx) mon_to_scb,
                 input int unsigned num_tests,
                 input real tolerance);
        this.drv_to_scb = drv_to_scb;
        this.mon_to_scb = mon_to_scb;
        this.tolerance  = tolerance;
        this.num_tests  = num_tests;
    endfunction

    task wrap_up();
        if(drv_to_scb.num() != mon_to_scb.num()) begin
            $display("ERROR: Different number of transactions. Driver sent %d vs Monitor sent %d\n",
                        this.drv_to_scb.num(),
                        this.mon_to_scb.num());

            drv_tx      = new[this.drv_to_scb.num()];
            mon_tx      = new[this.mon_to_scb.num()];

            // List transactions sent by Driver
            $display("Sent by Driver: ");
            for(int i=0; this.drv_to_scb.num(); i=i+1) begin
                this.drv_to_scb.get(this.drv_tx[i]);
                
                $display("Per: %t -- Duty: %t", this.drv_tx[i].exp_period, this.drv_tx[i].exp_duty_time);
            end

            // List transactions sent by Monitor
            $display("Sent by Monitor: ");
            for(int i=0; i < this.mon_to_scb.num(); i=i+1) begin
                this.mon_to_scb.get(this.mon_tx[i]);
                
                $display("Per: %t -- Duty: %t", this.mon_tx[i].period, this.mon_tx[i].duty_time);
            end
        end
        else begin
            this.num_tx = this.drv_to_scb.num(); 
            error_per   = new[this.num_tx];
            error_dutyc = new[this.num_tx];
            drv_tx      = new[this.num_tx];
            mon_tx      = new[this.num_tx];

            $display("PASS: Equal number of transactions: %d\n", this.num_tx);
            
            for(int i=0; i < num_tests; i=i+1) begin
                this.drv_to_scb.get(this.drv_tx[i]);
                this.mon_to_scb.get(this.mon_tx[i]);
                
                this.error_per[i]   = abs(this.mon_tx[i].period/this.drv_tx[i].exp_period-1);
                this.error_dutyc[i] = abs(this.mon_tx[i].duty_time/this.drv_tx[i].exp_duty_time-1);
                
                $display("Tx[%d] Error: %f | Error Expected: %f | Exp: %t -- Got: %t", 
                    i, 
                    this.error_per[i],
                    pwm_module_pkg::expected_error(32,this.drv_tx[i].freq), 
                    this.drv_tx[i].exp_period,     
                    this.mon_tx[i].period);
                $display("Tx[%d] Error: %f | Error Expected: %f | Exp: %t -- Got: %t", 
                    i, 
                    this.error_dutyc[i],
                    pwm_module_pkg::expected_error(32,this.drv_tx[i].freq), 
                    this.drv_tx[i].exp_duty_time, 
                    this.mon_tx[i].duty_time);
            end
        end 

        // Evaluating error values
        foreach(error_per[i]) begin
            if((error_per[i] >= pwm_module_pkg::expected_error(32,this.drv_tx[i].freq)+this.tolerance) ||
               (error_per[i] <= pwm_module_pkg::expected_error(32,this.drv_tx[i].freq)-this.tolerance)) begin
                this.num_error_per++;
                $display("Error on PERIOD of Tx[%d]: error is %f, but expected %f", i, error_per[i], pwm_module_pkg::expected_error(32,this.drv_tx[i].freq));
            end
        end
            
        foreach(error_dutyc[i]) begin
            if((error_dutyc[i] >= pwm_module_pkg::expected_error(32,this.drv_tx[i].freq)+this.tolerance) ||
               (error_dutyc[i] <= pwm_module_pkg::expected_error(32,this.drv_tx[i].freq)-this.tolerance)) begin
                this.num_error_dutyc++;
                $display("Error on DUTYC of Tx[%d]: error is %f, but expected %f", i, error_dutyc[i], pwm_module_pkg::expected_error(32,this.drv_tx[i].freq));
            end
        end
    endtask

    function int print_final_report();
        if(this.num_error_per) 
            $display("There were %d Period errors", this.num_error_per);
        if(this.num_error_dutyc) 
            $display("There were %d Dutyc errors", this.num_error_per);
        if(this.num_error_per == 0 && this.num_error_dutyc == 0) begin
            $display("\n\n ****** TEST PASSED :) ******\n\n");
            return 0;
        end
        else begin
            $display("\n\n ****** TEST FAILED :) ******\n\n");
            return -1;
        end
    endfunction
endclass
