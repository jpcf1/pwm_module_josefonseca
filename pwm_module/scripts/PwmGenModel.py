import numpy as np
import matplotlib.pyplot as plt

# System Parameters
class PwmGenModel():
    def __init__(self, arch="counter", N=8, MAX_N=64):
        self.MAX_R = 16
        self.MIN_R = 12
        self.MAX_K = 8
        self.MIN_K = 2
        self.fmax  = 2e3
        self.fstep = 250
        self.arch  = arch

    def config_arch_counter(self, N=8, MAX_N=64):
        self.N     = N
        self.MAX_N = MAX_N
        
        self.N_vals = [] 
        while N <= self.MAX_N:
            self.N_vals.append(N)
            N = N*2

        self.per_float    = np.zeros((len(self.N_vals), self.MAX_K-self.MIN_K+1))
        self.per_hex_word = np.zeros((len(self.N_vals), self.MAX_K-self.MIN_K+1), dtype=int)
        self.error        = np.zeros((len(self.N_vals), self.MAX_K-self.MIN_K+1))
        self.per_hex_word_bw = 0
    
    def config_arch_delayline(self, sweep_Nc):
        


    # Function that generates the period words such that the 
    # comparator with the timebase registers strobes for every period
    # of the requested word
    def gen_period_words(self):
        print(self.N_vals)
        for N in range(len(self.N_vals)):
            for K in range(self.MIN_K, self.MAX_K+1):
                self.per_float[N, K-self.MIN_K] = self.N_vals[N]/K
                self.per_hex_word[N, K-self.MIN_K] = np.round(self.per_float[N, K-self.MIN_K])

    # Error Function
    def eval_error_pwm(self, percent=False, gen_plot=False):
        if(self.arch == "counter"):
            if(percent):
                self.error = 100*np.abs(self.per_hex_word/self.per_float-1)
            else:
                self.error = np.abs(self.per_hex_word/self.per_float-1)

            for N in range(len(self.N_vals)):
                plt.plot(np.arange(self.MIN_K, self.MAX_K+1), self.error[N,], '.-', label=f"N={self.N_vals[N]}")
            plt.legend(loc="upper right")
            plt.grid(True)
            plt.savefig('../../report/figs/error_pwm_counter')
            plt.show()



    # Prints a Report
    def print_report(self):
        self.gen_period_words()
        self.eval_error_pwm(gen_plot=True)

        for R in range(self.MIN_R, self.MAX_R+1):
            for K in range(self.MIN_K, self.MAX_K+1):
                print(f"{K*self.fmax/self.MAX_K}, {R}, {self.per_hex_word[R-self.MIN_R,K-self.MIN_K]:X}, {self.d_error[R-self.MIN_R,K-self.MIN_K]}%")
        



# Generating values for delta_elem
pwmgen = PwmGenModel(arch="counter", N=8, MAX_N=64)
pwmgen.gen_period_words()
pwmgen.eval_error_pwm()
