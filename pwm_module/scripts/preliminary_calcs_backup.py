import numpy as np

# System Parameters
class SystemParam():
    def __init__(self, fcore):
        self.MAX_R = 16
        self.MIN_R = 12
        self.MAX_K = 8
        self.MIN_K = 2
        self.fmax  = 2e3
        self.fstep = 250
        
        self.d_float      = np.zeros((self.MAX_R-self.MIN_R+1, self.MAX_K-self.MIN_K+1))
        self.d_error      = np.zeros((self.MAX_R-self.MIN_R+1, self.MAX_K-self.MIN_K+1))
        self.per_float    = np.zeros((self.MAX_R-self.MIN_R+1, self.MAX_K-self.MIN_K+1))
        self.per_error    = np.zeros((self.MAX_R-self.MIN_R+1, self.MAX_K-self.MIN_K+1))
        self.per_hex_word = np.zeros((self.MAX_K-self.MIN_K+1, self.MAX_K-self.MIN_K+1))#, dtype=int)

    def eval_fcore(self):
        for R in range(self.MIN_R, self.MAX_R+1):
            for K in range(self.MIN_K, self.MAX_K+1):
                self.per_hex_word[R-self.MIN_R,K-self.MIN_K] = (2**(self.MAX_R+3-R))/K

        self.lcm   = np.lcm.reduce(self.per_hex_word)
        self.fcore = self.lcm
        print(f"The least common multiple for chosen config. is {self.lcm}, yielding f_core = {self.fcore/1e6}MHz")

    # Function that generates the period words such that the 
    # comparator with the timebase registers strobes for every period
    # of the requested word
    def gen_period_words(self):
        
        for R in range(self.MIN_R, self.MAX_R+1):
            for K in range(self.MIN_K, self.MAX_K+1):
                self.per_float[R-self.MIN_R,K-self.MIN_K] = np.round(self.fcore/(K*self.fmax*(2**(self.MAX_R-R-3))))
                self.per_hex_word[R-self.MIN_R,K-self.MIN_K] = int(self.per_float[R-self.MIN_R,K-self.MIN_K])-1
                self.d_float[R-self.MIN_R,K-self.MIN_K] = self.per_float[R-self.MIN_R,K-self.MIN_K]*(2**R) 
                
                if(self.per_hex_word[R-self.MIN_R,K-self.MIN_K] > 2**R-1):
                    print(f"ERROR: The requested period word would exceed the representation \
    capacity of the chosen resolution ({R} bits): {self.per_hex_word[R-self.MIN_R,K-self.MIN_K]} vs {2**R-1}")
        

    # Error Function
    def eval_error_pwm(self, percent=False):
        if(percent):
            self.per_error = 100*np.abs(np.round(self.per_float)/self.per_float-1)
            self.d_error   = 100*np.abs(np.round(self.d_float)/self.d_float-1)
        else:
            self.per_error = np.abs(np.round(self.per_float)/self.per_float-1)
            self.d_error   = np.abs(np.round(self.d_float)/self.d_float-1)

    # Prints a Report
    def print_report(self):
        self.gen_period_words()
        self.error_pwm()

        for R in range(self.MIN_R, self.MAX_R+1):
            for K in range(self.MIN_K, self.MAX_K+1):
                print(f"{K*self.fmax/self.MAX_K}, {R}, {self.per_hex_word[R-self.MIN_R,K-self.MIN_K]:X}, {self.d_error[R-self.MIN_R,K-self.MIN_K]}%")
        



# Generating values for delta_elem
pwmgen = SystemParam(fcore = 32.768e6)
pwmgen.eval_fcore()

