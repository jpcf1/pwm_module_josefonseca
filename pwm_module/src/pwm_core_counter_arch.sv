import pwm_module_pkg::*;

module pwm_core_counter_arch
    #(parameter logic ASYNC=0)
    (
        // Clocks and Resets
        input core_clk,
        input core_rst_n,

        // Configurations
        input             [2:0] freq_sel,
        input             [2:0] res_sel,
        input [MAX_PWM_RES-1:0] dutyc_sel,
        input                   polarity,
        input                   enable,

        // Status
        output logic [1:0] current_status,
        output logic       new_status_reg_strb,

        // PWM Output
        output logic       pwm_out
    );

    timeunit      1ns;
    timeprecision 1fs;

    localparam N=32;
    localparam PER_CNT_BW=$clog2(N);
    
    logic [MAX_PWM_RES-1:0] timebase_cnt;
    logic [PER_CNT_BW-1:0]  period_cnt;
    logic [PER_CNT_BW-1:0]  period_word;
    logic             [2:0] res_word;
    logic [MAX_PWM_RES-1:0] dutyc_word;
    logic                   polarity_reg;
    logic                   enable_reg;

    logic fbase_strobe;
    logic period_compare_strobe;
    logic dutyc_compare_strobe;

    typedef enum int unsigned {
        NORMAL_OPERATION,
        DUTYC_COMPARE,
        PERIOD_COMPARE
    } state_t;

    state_t state, next_state;

    // Conversion of the frequency selection to a period word
    always_ff @(posedge core_clk, negedge core_rst_n)
    begin : freq_decoding_and_sampling
        if(!core_rst_n) begin
            period_word <=  {PER_CNT_BW{1'b0}};
        end
        else if(enable_reg) begin
            if(period_compare_strobe) begin
                case(freq_sel)
                    F_500  : period_word <= PER_WORDS_LUT[ F_500];
                    F_750  : period_word <= PER_WORDS_LUT[ F_750];
                    F_1000 : period_word <= PER_WORDS_LUT[F_1000];
                    F_1250 : period_word <= PER_WORDS_LUT[F_1250];
                    F_1500 : period_word <= PER_WORDS_LUT[F_1500];
                    F_1750 : period_word <= PER_WORDS_LUT[F_1750];
                    F_2000 : period_word <= PER_WORDS_LUT[F_2000];
                    default:
                    begin
                        period_word <= PER_WORDS_LUT[ F_500];
                    end
                endcase
            end
            else begin
                period_word <= period_word;
            end
        end
        else
            period_word <= period_word;
    end : freq_decoding_and_sampling

    // Sampling the Duty Cycle word and Polarity Sel
    always_ff @(posedge core_clk, negedge core_rst_n)
    begin : dutyc_sampling
        if(!core_rst_n) begin
            dutyc_word   <= {MAX_PWM_RES{1'b1}};
            polarity_reg <= ACTIVE_HIGH;
            res_word     <= 3'b00;
            enable_reg   <= 1'b0;
        end
        else if(period_compare_strobe) begin
            dutyc_word   <= dutyc_sel;
            polarity_reg <= polarity;
            res_word     <= res_sel;
            enable_reg   <= enable;
        end
        else begin
            dutyc_word <= dutyc_word;
            polarity_reg <= polarity_reg;
            res_word     <= res_word;
            enable_reg   <= enable_reg;
        end
    end  : dutyc_sampling
        

    // Input clock divider
    always_ff @(posedge core_clk, negedge core_rst_n)
    begin : fcore_clk_divider
        if(!core_rst_n) 
            period_cnt <= 'd0;
        else if(enable_reg) begin
            if(fbase_strobe)
                period_cnt <= 'd0;
            else
                period_cnt <= period_cnt + 1'b1;
        end
        else begin
            period_cnt <= period_cnt;
        end
    end : fcore_clk_divider
    
    // Timebase strobe generator
    assign fbase_strobe = (period_cnt == period_word-1);

    // Timebase generation counter
    always_ff @(posedge core_clk, negedge core_rst_n)
    begin : timebase_cnt_generation
        if(!core_rst_n) begin
            timebase_cnt <= {MAX_PWM_RES{1'b0}};
        end
        else if(enable_reg) begin
            if(fbase_strobe) begin
                timebase_cnt <= timebase_cnt + 1'b1;
            end
            else begin
                timebase_cnt <= timebase_cnt;
            end
        end
        else begin
            timebase_cnt <= timebase_cnt;
        end
    end : timebase_cnt_generation

    // Generation of Period Compare Strobe
    assign period_compare_strobe = ({MAX_PWM_RES{1'b0}} == (timebase_cnt));

    // Generation of Period Compare Strobe
    assign dutyc_compare_strobe  = (dutyc_word == (timebase_cnt >> res_word));

    // Generation of Status signal
    assign current_status[0] = pwm_out;
    assign current_status[1] = polarity_reg;
    assign new_status_reg_strb = period_compare_strobe | dutyc_compare_strobe; 

    // Generation of the output PWM Signal
    always_ff @(posedge core_clk, negedge core_rst_n) 
    begin : output_generation
        if(!core_rst_n) 
            pwm_out <= ~polarity_reg;
        else begin
            if(fbase_strobe) begin
                if(period_compare_strobe) begin
                    pwm_out <= polarity_reg;
                end
                else if(dutyc_compare_strobe) begin
                    pwm_out <= ~polarity_reg;
                end
            end
            else begin
                pwm_out <= pwm_out;
            end
        end
    end

endmodule

    

    
