module delay_line_model
    #(
        DELAY_VAL=1ns,
        N_TAPS=64
    )
    (
        input  in,
        output logic [N_TAPS-1:0] out
    );

    always begin
        for(integer n=0; n < N_TAPS; n++) begin
            out[n] = #(DELAY_VAL*n) in; 
        end
    end
endmodule


