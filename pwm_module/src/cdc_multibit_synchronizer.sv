module cdc_multibit_synchronizer #(
    DATA_BW=16,
    SYNC_PIPELINE_DEPTH=4
    )
    (
        // The Write side (clk_wr domain)
        input               clk_wr,
        input [DATA_BW-1:0] data_in,
        input               data_in_new,
        output logic        data_in_done,

        // The Read side (clk_rd domain)
        input                      clk_rd,
        output logic [DATA_BW-1:0] data_out,
        output logic               data_out_rdy
    );

    // The enable/acknowledge signal synchronizer pipeline
    logic [SYNC_PIPELINE_DEPTH-1:0] data_out_rdy_reg;
    logic [SYNC_PIPELINE_DEPTH-1:0] data_in_done_reg;

    // data_out_rdy Pipeline Synchronizer
    always_ff @(posedge clk_rd)
    begin : data_out_rdy_gen
        data_out_rdy_reg <= {data_out_rdy_reg[SYNC_PIPELINE_DEPTH-2:0], data_in_new};
    end : data_out_rdy_gen
   
    // Rising edge detector for data_out_rdy
    assign data_out_rdy = data_out_rdy_reg[SYNC_PIPELINE_DEPTH-1] && (~data_out_rdy_reg[SYNC_PIPELINE_DEPTH-2]);

    // data_out_rdy Pipeline Synchronizer
    always_ff @(posedge clk_wr)
    begin : data_in_done_gen
        data_in_done_reg <= {data_in_done_reg[SYNC_PIPELINE_DEPTH-2:0], data_out_rdy};
    end : data_in_done_gen
   
    // Rising edge detector for data_out_rdy
    assign data_in_done = data_in_done_reg[SYNC_PIPELINE_DEPTH-1] && (~data_in_done_reg[SYNC_PIPELINE_DEPTH-2]);

    // Pass-through of data_in->data_out
    assign data_out = data_in;

endmodule
