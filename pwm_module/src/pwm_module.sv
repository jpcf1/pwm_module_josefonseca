
import pwm_module_pkg::*;

module pwm_module 
    #(parameter int   APB_SLV_DW = 16,
      parameter int   APB_SLV_AW = 32,
      parameter logic ASYNC=0)
    (
        // APB Side
        input                         pclk,
        input                         preset_n,
        input        [APB_SLV_AW-1:0] paddr,
        input                         pwrite,
        input                         psel,
        input                         penable,
        input        [APB_SLV_DW-1:0] pwdata,
        input      [APB_SLV_DW/8-1:0] pstrb,
        output logic [APB_SLV_DW-1:0] prdata,
        output logic                  pready,
        output logic                  pslverr,

        // PWM Clock
        input core_clk,
        input core_rst_n,

        // PWM Output
        output logic                 pwm_o
    );
    
    // Connection Wires
    logic        en;
    logic        polarity;
    logic  [2:0] res_sel;
    logic  [2:0] freq_sel;
    logic [15:0] dutyc_sel;
    logic        apb_new_data_strb;

    // Input registers to PWM Core
    logic        en_reg               [2];
    logic        polarity_reg         [2];
    logic  [2:0] res_sel_reg          [2];
    logic  [2:0] freq_sel_reg         [2];
    logic [15:0] dutyc_sel_reg        [2];
    logic        apb_new_data_strb_reg[2];
    
    // Output registers to PWM Core
    logic [1:0] status_reg[3];
    logic       new_status_reg_strb_sync;
    logic       new_status_reg_strb;

    // Instantiation of the APB slave
    apb_slave #(
        .APB_SLV_DW(APB_SLV_DW),
        .APB_SLV_AW(APB_SLV_AW)
    ) apb_slave_inst (
        .pclk(pclk),
        .preset_n(preset_n),
        .paddr(paddr),
        .pwrite(pwrite),
        .psel(psel),
        .penable(penable),
        .pwdata(pwdata),
        .pstrb(pstrb),
        .prdata(prdata),
        .pslverr(pslverr),
        .pready(pready),

        .en(en),
        .polarity(polarity),
        .res_sel(res_sel),
        .freq_sel(freq_sel),
        .dutyc_sel(dutyc_sel),
        .apb_new_data_strb(apb_new_data_strb),
        .apb_new_data_done(apb_new_data_done),

        .status_reg_val (status_reg[2]),
        .status_reg_strb(new_status_reg_strb)
    );

    // Instantiation of the PWM Core
    pwm_core_counter_arch 
    pwm_core_inst (
        .core_clk(core_clk),
        .core_rst_n(core_rst_n),
        
        .freq_sel(freq_sel_reg[1]),
        .res_sel(res_sel_reg[1]),
        .dutyc_sel(dutyc_sel_reg[1]),
        .polarity(polarity_reg[1]),
        .enable(en_reg[1]),
        .pwm_out(pwm_o),

        .current_status(status_reg[0]),
        .new_status_reg_strb(new_status_reg_strb)
    );

    // Clock domain crossing of the register values
    generate
        case(ASYNC)
            1:
            begin
                cdc_multibit_synchronizer #(
                    .DATA_BW(16),
                    .SYNC_PIPELINE_DEPTH(3)
                ) cdc_pwm_cfg1_reg (
                    .clk_wr(pclk),
                    .data_in(dutyc_sel),
                    .data_in_new(apb_new_data_strb),
                    .data_in_done(apb_new_data_done),

                    .clk_rd(core_clk),
                    .data_out(dutyc_sel_reg[0]),
                    .data_out_rdy(pwm_new_data_strb)
                );

                cdc_multibit_synchronizer #(
                    .DATA_BW(8),
                    .SYNC_PIPELINE_DEPTH(3)
                ) cdc_pwm_cfg2_reg (
                    .clk_wr(pclk),
                    .data_in({en, polarity, res_sel, freq_sel}),
                    .data_in_new(apb_new_data_strb),
                    .data_in_done(apb_new_data_done),

                    .clk_rd(core_clk),
                    .data_out({en_reg[0], polarity_reg[0], res_sel_reg[0], freq_sel_reg[0]}),
                    .data_out_rdy(pwm_new_data_strb)
                );

                cdc_multibit_synchronizer #(
                    .DATA_BW(2),
                    .SYNC_PIPELINE_DEPTH(3)
                ) cdc_pwm_stat_reg (
                    .clk_wr(core_clk),
                    .data_in(status_reg[0]),
                    .data_in_new(new_status_reg_strb),
                    .data_in_done(),

                    .clk_rd(pclk),
                    .data_out(status_reg[1]),
                    .data_out_rdy(new_status_reg_strb_sync)
                );
            end

            0:
            begin
                assign status_reg[1]    = status_reg[0];            
                assign dutyc_sel_reg[0] = dutyc_sel;            
                assign {en_reg[0], polarity_reg[0], res_sel_reg[0], freq_sel_reg[0]} = {en, polarity, res_sel, freq_sel};
            end
        endcase
    endgenerate

    // Resampling and data is available at the PWM output
    always_ff @(posedge core_clk, negedge core_rst_n)
    begin : pwm_regs_latching
        if(!core_rst_n) begin
            dutyc_sel_reg [1] <= {MAX_R{1'b0}}; 
            en_reg        [1] <= 1'b0;
            polarity_reg  [1] <= ACTIVE_LOW;
            res_sel_reg   [1] <= {PWM_CFG2_RES_SEL_BW {1'b0}};
            freq_sel_reg  [1] <= {PWM_CFG2_FREQ_SEL_BW{1'b0}};
            status_reg    [2] <= 2'b00;
        end
        else begin
            dutyc_sel_reg [1] <= dutyc_sel_reg[0];
            en_reg        [1] <= en_reg[0];
            polarity_reg  [1] <= polarity_reg[0];
            res_sel_reg   [1] <= res_sel_reg[0];
            freq_sel_reg  [1] <= freq_sel_reg[0];
            status_reg    [2] <= status_reg[1];
        end
    end : pwm_regs_latching
endmodule

