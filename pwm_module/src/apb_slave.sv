
import pwm_module_pkg::*;

module apb_slave 
    #(APB_SLV_DW = 16,
      APB_SLV_AW = 32)
    (
        // APB Side
        input                         pclk,
        input                         preset_n,
        input        [APB_SLV_AW-1:0] paddr,
        input                         pwrite,
        input                         psel,
        input                         penable,
        input        [APB_SLV_DW-1:0] pwdata,
        input      [APB_SLV_DW/8-1:0] pstrb,
        output logic [APB_SLV_DW-1:0] prdata,
        output logic                  pslverr,
        output logic                  pready,

        // Internal Side
        input               apb_new_data_done,
        output logic        apb_new_data_strb,
        output logic        en,
        output logic        polarity,
        output logic  [2:0] res_sel,
        output logic  [2:0] freq_sel,
        output logic [15:0] dutyc_sel,

        input logic   [1:0] status_reg_val,
        input logic         status_reg_strb
    );

    // Registers
    logic [APB_SLV_DW-1:0] pwm_cfg1_reg;
    logic [APB_SLV_DW-1:0] pwm_cfg2_reg;

    // State tags
    typedef enum int unsigned {
        SETUP_PHASE,
        DATA_WR_PHASE,
        DATA_RD_PHASE,
        PSLVERR_PHASE
    } state_t;

    // State Variables
    state_t state, next_state;

    // FSM - Sequential Part
    always_ff @(posedge pclk, negedge preset_n)
    begin : fsm_sequential
        if(!preset_n)
            state <= SETUP_PHASE;
        else
            state <= next_state;
    end : fsm_sequential

    // FSM - Combinational Part, next state generation logic
    always_comb
    begin : fsm_next_state_gen
        
        // Since all Writes and Reads take one clock cycle only,
        // if we keep this default assignment, then we don't
        // need to repeat it for the DATA_WR_PHASE and DATA_RD_PHASE
        next_state = SETUP_PHASE;

        if(!preset_n) begin
            next_state = SETUP_PHASE;
        end
        else begin
            case(state)
                SETUP_PHASE:
                begin
                    if(psel && !penable) begin
                        if(paddr > PWM_STATUS_REG || 
                           pstrb == 2'b11         ||
                          (!pwrite && pstrb != 2'b00)) begin
                            // Invalid memory access
                            next_state = PSLVERR_PHASE;
                        end
                        else begin
                            if(pwrite) 
                                next_state = DATA_WR_PHASE;
                            else if(!pwrite)
                                next_state = DATA_RD_PHASE;
                        end
                    end
                end
            endcase
        end
    end : fsm_next_state_gen
        
    // The register access
    always_ff @(posedge pclk, negedge preset_n)
    begin : regs_access_wr
        if(!preset_n) begin
            pwm_cfg1_reg <= 16'd0;
            pwm_cfg2_reg <= 16'd0;
        end
        else begin
            if(psel && penable && pwrite && state == DATA_WR_PHASE) 
                if(paddr == PWM_CFG1_DUTYC_LO_REG ||
                   paddr == PWM_CFG1_DUTYC_HI_REG) 
                begin
                    pwm_cfg1_reg[ paddr[0]*8 +: 8] <= pwdata[7:0];
                    //pwm_cfg1_reg[ paddr[0]*8 +: 8] <= pwdata[pstrb*8 +: 8];
                    pwm_cfg1_reg[(!paddr[0])*8 +: 8] <= pwm_cfg1_reg[(!paddr[0])*8 +: 8];
                    pwm_cfg2_reg <= pwm_cfg2_reg;
                end
                else if(paddr == PWM_CFG2_RES_FREQ_REG ||
                        paddr == PWM_CFG2_CTRL_REG) 
                begin
                    pwm_cfg1_reg <= pwm_cfg1_reg;
                    // last bit is not writeable
                    pwm_cfg2_reg[ paddr[0]*8 +: 8] <= pwdata[7:0];
                    //pwm_cfg2_reg[ paddr[1]*8 +: 8] <={1'b0, pwdata[pstrb*8 +: 7]};
                    pwm_cfg2_reg[(!paddr[0])*8 +: 8] <= pwm_cfg2_reg[(!paddr[0])*8 +: 8];
                end
            else begin
                pwm_cfg1_reg <= pwm_cfg1_reg;
                pwm_cfg2_reg <= pwm_cfg2_reg;
            end
        end
    end : regs_access_wr

    always_ff @(posedge pclk, negedge preset_n)
    begin : reg_access_rd
        if(psel && penable && !pwrite) 
            if(paddr == PWM_STATUS_REG) 
                prdata <= status_reg_val;
            else
                prdata <= prdata;

    end : reg_access_rd

    // Mapping registers to internal signals
    assign en        = pwm_cfg2_reg[PWM_CFG2_EN_OFFSET       +: PWM_CFG2_EN_BW      ];
    assign polarity  = pwm_cfg2_reg[PWM_CFG2_POL_OFFSET      +: PWM_CFG2_POL_BW     ];
    assign freq_sel  = pwm_cfg2_reg[PWM_CFG2_FREQ_SEL_OFFSET +: PWM_CFG2_FREQ_SEL_BW];
    assign res_sel   = pwm_cfg2_reg[PWM_CFG2_RES_SEL_OFFSET  +: PWM_CFG2_RES_SEL_BW ];
    assign dutyc_sel = pwm_cfg1_reg;

    // Generation of new_data strobe
    always_ff @(posedge pclk, negedge preset_n)
    begin : apb_new_data_strb_gen
        if(!preset_n)
            apb_new_data_strb <= 1'b0;
        else begin
            // Since the FSM, by design, stays in this state only one
            // clock cycle, this will generate one pulse, of pclk length
            // per write access, and this will be delayed by one clock
            // cycle so that the data is available by the time we issue this signal
            apb_new_data_strb <= (state == DATA_WR_PHASE);
        end
    end : apb_new_data_strb_gen
    
    // Generation of pready and pslverr
    assign pslverr = (state == PSLVERR_PHASE);
    assign pready  = (state != SETUP_PHASE);

endmodule
