
package pwm_module_pkg;

    timeunit      1ns;
    timeprecision 1fs;

    // ---------------------------------- //
    // Data Types and Enumeration Symbols //
    // ---------------------------------- //
    parameter logic [31:0] PWM_CFG1_DUTYC_LO_REG = 32'd0;
    parameter logic [31:0] PWM_CFG1_DUTYC_HI_REG = 32'd1;
    parameter logic [31:0] PWM_CFG2_RES_FREQ_REG = 32'd2;
    parameter logic [31:0] PWM_CFG2_CTRL_REG     = 32'd3;
    parameter logic [31:0] PWM_STATUS_REG        = 32'd4;

    parameter int unsigned PWM_CFG2_EN_OFFSET       = 8; 
    parameter int unsigned PWM_CFG2_EN_BW           = 1;
    parameter int unsigned PWM_CFG2_POL_OFFSET      = 6;
    parameter int unsigned PWM_CFG2_POL_BW          = 1;
    parameter int unsigned PWM_CFG2_FREQ_SEL_OFFSET = 0;
    parameter int unsigned PWM_CFG2_FREQ_SEL_BW     = 3; 
    parameter int unsigned PWM_CFG2_RES_SEL_OFFSET  = 3;
    parameter int unsigned PWM_CFG2_RES_SEL_BW      = 3;

    // ---------------------------------- //
    // Data Types and Enumeration Symbols //
    // ---------------------------------- //
    typedef enum logic [2:0] {
        F_500,
        F_750,
        F_1000,
        F_1250,
        F_1500,
        F_1750,
        F_2000
    } period_divider_t;

    typedef enum logic [2:0] {
        R_16, 
        R_15, 
        R_14, 
        R_13, 
        R_12
    } resolution_t;

    typedef enum logic {
       ACTIVE_HIGH=1,
       ACTIVE_LOW =0
    } polarity_t;

    // ---------------------------------- //
    //         General Parameters         //
    // ---------------------------------- //
    parameter MAX_PWM_RES = 16; 
    parameter FMAX  = 2000;  // Hz
    parameter MAX_R = 16;
    parameter MIN_R = 12;
    parameter NUM_R = MAX_R-MIN_R+1;
    parameter MAX_K = 8;
    parameter MIN_K = 2;
    parameter NUM_K = MAX_K-MIN_K+1;
    parameter LCM   = 840;

    // ---------------------------------- //
    // Associative Arrays and Useful LUTs //
    // ---------------------------------- //
    logic [4:0] PER_WORDS_LUT [period_divider_t] = '{
        F_500   : 5'd16,
        F_750   : 5'd11,
        F_1000  : 5'd8,
        F_1250  : 5'd6,
        F_1500  : 5'd5,
        F_1750  : 5'd5,
        F_2000  : 5'd4
    };
    

    parameter time DELAY_VAL_LUT[NUM_K] = {
       30.51757813ns, 
       20.34505208ns, 
       15.25878906ns, 
       12.20703125ns, 
       10.17252604ns, 
        8.71930804ns, 
        7.62939453ns
    };

    int R_to_nbits[resolution_t] = '{
        R_16 : 16,
        R_15 : 15,
        R_14 : 14,
        R_13 : 13,
        R_12 : 12
    };
        
    int freq_to_Hz[period_divider_t] = '{
        F_500  : 500.0, 
        F_750  : 750.0,
        F_1000 : 1000.0,
        F_1250 : 1250.0,
        F_1500 : 1500.0,
        F_1750 : 1750.0,
        F_2000 : 2000.0
    };

    int freq_to_k[period_divider_t] = '{
        F_500  : 2, 
        F_750  : 3,
        F_1000 : 4,
        F_1250 : 5,
        F_1500 : 6,
        F_1750 : 7,
        F_2000 : 8
    };

    // ---------------------------------- //
    //          Useful Functions          //
    // ---------------------------------- //
    function realtime freq_to_time(period_divider_t freq);
        return (1s)/(freq_to_Hz[freq]);
    endfunction

    function realtime dutyc_to_time(logic [15:0] dutyc, period_divider_t freq, resolution_t res);
        return dutyc * (freq_to_time(freq))/(2**(R_to_nbits[res]));
    endfunction

    function real abs(real num);
        if(num < 0)
            return (0-num);
        return num;
    endfunction

    function real expected_error(input int N, period_divider_t freq_sel);
        automatic int  k = freq_to_k[freq_sel]; 
        automatic int  rounded = (1.0*N)/k;
        automatic real out = abs(rounded*(1.0*k)/N - 1.0);
        //$display("Rounded: %d | Non-Rounded: %f | Out: %f",rounded, (1.0*N)/k, out);
        return out;
    endfunction
endpackage


