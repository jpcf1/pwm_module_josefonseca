import pwm_module_pkg::*;

module pwm_core_delayline_arch
    #(parameter logic ASYNC=0)
    (
        // Clocks and Resets
        input core_clk,
        input core_rst_n,

        // Configurations
        input             [2:0] freq_sel,
        input             [2:0] res_sel,
        input [MAX_PWM_RES-1:0] dutyc_sel,
        input                   polarity, 
        input                   enable,

        // PWM Output
        output logic pwm_out
    );

    localparam COARSE_DELAY_RES = 10;
    localparam FINE_DELAY_RES   =  6;
    localparam PER_CNT_BW       =  $rtoi($clog2(LCM/MIN_K));
    
    logic [COARSE_DELAY_RES-1:0] coarse_delay_cnt;
    logic       [PER_CNT_BW-1:0] period_cnt;
    logic       [PER_CNT_BW-1:0] period_word;

    logic fbase_strobe;
    logic period_compare;
    logic dutyc_compare;
    logic coarse_delay_pulse;
    logic fine_delay_pulse [NUM_K];

    // Splitting the duty cycle register input into HI and LO parts
    logic   [FINE_DELAY_RES-1:0] dutyc_lo;
    logic [COARSE_DELAY_RES-1:0] dutyc_hi;

    assign dutyc_hi = dutyc_sel[$high(dutyc_sel) -: COARSE_DELAY_RES];
    assign dutyc_lo = dutyc_sel[               0 +: FINE_DELAY_RES];

    // Conversion of the frequency selection to a period word
    always_ff @(posedge core_clk, negedge core_rst_n)
    begin
        if(core_rst_n) begin
            period_word <=  {PER_CNT_BW{1'b0}};
        end
        else begin
            case(freq_sel)
                F_500  : period_word = PER_WORDS_LUT[ F_500];
                F_750  : period_word = PER_WORDS_LUT[ F_750];
                F_1000 : period_word = PER_WORDS_LUT[F_1000];
                F_1250 : period_word = PER_WORDS_LUT[F_1250];
                F_1500 : period_word = PER_WORDS_LUT[F_1500];
                F_1750 : period_word = PER_WORDS_LUT[F_1750];
                F_2000 : period_word = PER_WORDS_LUT[F_2000];
                default:
                begin
                    period_word = PER_WORDS_LUT[ F_500];
                end
            endcase
        end
    end

    // Input clock divider
    always_ff @(posedge core_clk, negedge core_rst_n)
    begin : fcore_clk_divider
        if(!core_rst_n) 
            period_cnt <= 'd0;
        else
            if(fbase_strobe)
                period_cnt <= 'd0;
            else
                period_cnt <= period_cnt + 1'b1;
    end : fcore_clk_divider
    
    // Timebase strobe generator
    assign fbase_strobe = (period_cnt == period_word);

    // Coarse-delay generation counter
    always_ff @(posedge core_clk, negedge core_rst_n)
    begin : timebase_cnt_generation
        if(core_rst_n) 
            coarse_delay_cnt <= {MAX_PWM_RES{1'b0}};
        else if(fbase_strobe)
            coarse_delay_cnt <= coarse_delay_cnt + 1'b1;
        else
            coarse_delay_cnt <= coarse_delay_cnt;
    end : timebase_cnt_generation

    // Generation of the period strobe
    assign period_compare = ({COARSE_DELAY_RES{1'b1}} == coarse_delay_cnt); 

    // Generation of coarse-delayed pulse
    assign coarse_delay_pulse = (dutyc_hi == coarse_delay_cnt);

    // Instantiation of the Delay Lines for each of the k in [2,8] frequencies
    genvar k;
    generate
        for(k=0; k < NUM_K; k++) begin
            delay_line_model #(
                .DELAY_VAL(DELAY_VAL_LUT[k]),
                .N_TAPS(FINE_DELAY_RES)
            )
            delay_line_inst (
                .in (coarse_delay_pulse),
                .out(fine_delay_pulse[k])
            );
        end
    endgenerate

    // Generation of the output PWM Signal
endmodule

    

    
