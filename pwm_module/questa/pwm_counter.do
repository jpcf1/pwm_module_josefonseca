onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider Inputs
add wave -noupdate /tb_pwm_core/pwm_core_inst/core_clk
add wave -noupdate /tb_pwm_core/pwm_core_inst/core_rst_n
add wave -noupdate /tb_pwm_core/pwm_core_inst/freq_sel
add wave -noupdate /tb_pwm_core/pwm_core_inst/res_sel
add wave -noupdate /tb_pwm_core/pwm_core_inst/dutyc_sel
add wave -noupdate /tb_pwm_core/pwm_core_inst/polarity
add wave -noupdate /tb_pwm_core/pwm_core_inst/enable
add wave -noupdate -divider {Internal Signals}
add wave -noupdate /tb_pwm_core/pwm_core_inst/timebase_cnt
add wave -noupdate /tb_pwm_core/pwm_core_inst/period_cnt
add wave -noupdate /tb_pwm_core/pwm_core_inst/period_word
add wave -noupdate /tb_pwm_core/pwm_core_inst/period_compare_strobe
add wave -noupdate /tb_pwm_core/pwm_core_inst/dutyc_word
add wave -noupdate /tb_pwm_core/pwm_core_inst/dutyc_compare_strobe
add wave -noupdate /tb_pwm_core/pwm_core_inst/polarity_reg
add wave -noupdate /tb_pwm_core/pwm_core_inst/fbase_strobe
add wave -noupdate -divider Outputs
add wave -noupdate /tb_pwm_core/pwm_core_inst/pwm_out
add wave -noupdate -divider {TestBench Signals}
add wave -noupdate /tb_pwm_core/core_clk
add wave -noupdate /tb_pwm_core/drv_finish
add wave -noupdate /tb_pwm_core/mon_finish
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {499924817500 fs} 0} {{Cursor 2} {305041770974 fs} 0}
quietly wave cursor active 1
configure wave -namecolwidth 307
configure wave -valuecolwidth 140
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {499907804425 fs} {499941830576 fs}
