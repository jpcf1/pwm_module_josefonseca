onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider Inputs
add wave -noupdate -expand -group {APB Interface} /tb_pwm_module/pwm_module_inst/pclk
add wave -noupdate -expand -group {APB Interface} /tb_pwm_module/pwm_module_inst/preset_n
add wave -noupdate -expand -group {APB Interface} /tb_pwm_module/pwm_module_inst/paddr
add wave -noupdate -expand -group {APB Interface} /tb_pwm_module/pwm_module_inst/pwrite
add wave -noupdate -expand -group {APB Interface} /tb_pwm_module/pwm_module_inst/psel
add wave -noupdate -expand -group {APB Interface} /tb_pwm_module/pwm_module_inst/penable
add wave -noupdate -expand -group {APB Interface} /tb_pwm_module/pwm_module_inst/pwdata
add wave -noupdate -expand -group {APB Interface} /tb_pwm_module/pwm_module_inst/prdata
add wave -noupdate -expand -group {APB Interface} /tb_pwm_module/pwm_module_inst/pready
add wave -noupdate -expand -group {APB Interface} /tb_pwm_module/pwm_module_inst/pslverr
add wave -noupdate -expand -group {PWM Interface} /tb_pwm_module/pwm_module_inst/core_clk
add wave -noupdate -expand -group {PWM Interface} /tb_pwm_module/pwm_module_inst/core_rst_n
add wave -noupdate -expand -group {PWM Interface} /tb_pwm_module/pwm_module_inst/pwm_o
add wave -noupdate -divider {APB Slave}
add wave -noupdate -expand -group {APB Interface Internal} /tb_pwm_module/pwm_module_inst/apb_slave_inst/pclk
add wave -noupdate -expand -group {APB Interface Internal} /tb_pwm_module/pwm_module_inst/apb_slave_inst/preset_n
add wave -noupdate -expand -group {APB Interface Internal} /tb_pwm_module/pwm_module_inst/apb_slave_inst/paddr
add wave -noupdate -expand -group {APB Interface Internal} /tb_pwm_module/pwm_module_inst/apb_slave_inst/pwrite
add wave -noupdate -expand -group {APB Interface Internal} /tb_pwm_module/pwm_module_inst/apb_slave_inst/psel
add wave -noupdate -expand -group {APB Interface Internal} /tb_pwm_module/pwm_module_inst/apb_slave_inst/penable
add wave -noupdate -expand -group {APB Interface Internal} /tb_pwm_module/pwm_module_inst/apb_slave_inst/pstrb
add wave -noupdate -expand -group {APB Interface Internal} /tb_pwm_module/pwm_module_inst/apb_slave_inst/pslverr
add wave -noupdate -expand -group {APB Interface Internal} /tb_pwm_module/pwm_module_inst/apb_slave_inst/pready
add wave -noupdate -expand -group {APB Interface Internal} /tb_pwm_module/pwm_module_inst/apb_slave_inst/pwdata
add wave -noupdate -expand -group {APB Interface Internal} /tb_pwm_module/pwm_module_inst/apb_slave_inst/prdata
add wave -noupdate -expand -group {State Machine} /tb_pwm_module/pwm_module_inst/apb_slave_inst/state
add wave -noupdate -expand -group {State Machine} /tb_pwm_module/pwm_module_inst/apb_slave_inst/next_state
add wave -noupdate -expand -group Registers /tb_pwm_module/pwm_module_inst/apb_slave_inst/status_reg_strb
add wave -noupdate -expand -group Registers /tb_pwm_module/pwm_module_inst/apb_slave_inst/status_reg_val
add wave -noupdate -expand -group Registers /tb_pwm_module/pwm_module_inst/apb_slave_inst/pwm_cfg1_reg
add wave -noupdate -expand -group Registers /tb_pwm_module/pwm_module_inst/apb_slave_inst/pwm_cfg2_reg
add wave -noupdate -expand -group {Output Values} /tb_pwm_module/pwm_module_inst/apb_slave_inst/en
add wave -noupdate -expand -group {Output Values} /tb_pwm_module/pwm_module_inst/apb_slave_inst/polarity
add wave -noupdate -expand -group {Output Values} /tb_pwm_module/pwm_module_inst/apb_slave_inst/res_sel
add wave -noupdate -expand -group {Output Values} /tb_pwm_module/pwm_module_inst/apb_slave_inst/freq_sel
add wave -noupdate -expand -group {Output Values} /tb_pwm_module/pwm_module_inst/apb_slave_inst/dutyc_sel
add wave -noupdate -expand -group {Register Strobes} /tb_pwm_module/pwm_module_inst/apb_slave_inst/apb_new_data_done
add wave -noupdate -expand -group {Register Strobes} /tb_pwm_module/pwm_module_inst/apb_slave_inst/apb_new_data_strb
add wave -noupdate -divider {CDC Sync CFG1}
add wave -noupdate -divider {PWM Core}
add wave -noupdate -expand -group {PWM Core Signals} /tb_pwm_module/pwm_module_inst/pwm_core_inst/core_clk
add wave -noupdate -expand -group {PWM Core Signals} /tb_pwm_module/pwm_module_inst/pwm_core_inst/core_rst_n
add wave -noupdate -expand -group {PWM Core Signals} /tb_pwm_module/pwm_module_inst/pwm_core_inst/freq_sel
add wave -noupdate -expand -group {PWM Core Signals} /tb_pwm_module/pwm_module_inst/pwm_core_inst/res_sel
add wave -noupdate -expand -group {PWM Core Signals} /tb_pwm_module/pwm_module_inst/pwm_core_inst/dutyc_sel
add wave -noupdate -expand -group {PWM Core Signals} /tb_pwm_module/pwm_module_inst/pwm_core_inst/polarity
add wave -noupdate -expand -group {PWM Core Signals} /tb_pwm_module/pwm_module_inst/pwm_core_inst/enable
add wave -noupdate -expand -group {PWM Core Signals} /tb_pwm_module/pwm_module_inst/pwm_core_inst/timebase_cnt
add wave -noupdate -expand -group {PWM Core Signals} /tb_pwm_module/pwm_module_inst/pwm_core_inst/fbase_strobe
add wave -noupdate -expand -group {PWM Core Signals} /tb_pwm_module/pwm_module_inst/pwm_core_inst/period_compare_strobe
add wave -noupdate -expand -group {PWM Core Signals} /tb_pwm_module/pwm_module_inst/pwm_core_inst/dutyc_compare_strobe
add wave -noupdate -expand -group {PWM Core Signals} /tb_pwm_module/pwm_module_inst/pwm_core_inst/period_word
add wave -noupdate -expand -group {PWM Core Signals} /tb_pwm_module/pwm_module_inst/pwm_core_inst/dutyc_word
add wave -noupdate -expand -group {PWM Core Signals} /tb_pwm_module/pwm_module_inst/pwm_core_inst/pwm_out
add wave -noupdate -expand -group {PWM Core Signals} /tb_pwm_module/pwm_module_inst/pwm_core_inst/current_status
add wave -noupdate -divider Interconnections
add wave -noupdate -expand /tb_pwm_module/pwm_module_inst/dutyc_sel_reg
add wave -noupdate -divider Events
add wave -noupdate -expand -group Events /tb_pwm_module/drv_finish
add wave -noupdate -expand -group Events /tb_pwm_module/mon_finish
add wave -noupdate -expand -group Events /tb_pwm_module/drv_tx_done
add wave -noupdate -expand /tb_pwm_module/pwm_module_inst/status_reg
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 2} {13245777 fs} 0}
quietly wave cursor active 1
configure wave -namecolwidth 307
configure wave -valuecolwidth 140
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 fs} {22908316 fs}
