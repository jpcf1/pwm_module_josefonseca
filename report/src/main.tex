\documentclass[a4paper, onecolumn, 10pt]{article}

%%--Packages Opcionais--%%
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{epstopdf}
\usepackage{textcomp}
\usepackage{amsmath}
\usepackage{framed}
\usepackage{amstext}
\usepackage{SIunits}
\usepackage{tikz}
\usepackage{pgfplots}
\usepackage{float}
\usepackage[scale=0.75]{geometry}


\usepackage{color}
\usepackage[mathscr]{euscript} %%fonte caligráfica

%%--Tipos de Letra--%%
\usepackage{tgcursor} %para typewriter

%%--Cabeçalho de Rodapé--%%
\usepackage{fancyhdr}
\pagestyle{fancy}
\cfoot{\thepage}
\rfoot{}
\lhead{}
\chead{}
\rhead{}
\renewcommand{\headrulewidth}{0.5pt}
\renewcommand{\footrulewidth}{0.5pt}

%% Useful shortcuts for pieces of text that are often repeated
\newcommand{\khz}{~\kilo\hertz}
\newcommand{\mhz}{~\mega\hertz}
\newcommand{\ghz}{~\giga\hertz}
\newcommand{\fsel}{f_{\text{sel}}}
\newcommand{\fselk}{f_{\text{sel}}(k)}
\newcommand{\fmax}{f_{\text{max}}}
\newcommand{\fcore}{f_{\text{core}}}
\newcommand{\fbase}{f_{\text{base}}}
\newcommand{\fbasek}{f_{\text{base}}(k)}
\newcommand{\lcm}{\text{LCM}}
\newcommand{\logt}{\text{log}_2}
\newcommand{\eqv}{\Leftrightarrow}
\newcommand{\Rmax}{R_{\text{max}}}
\newcommand{\Tbasea}{T_{\text{base}}^{'}}
\newcommand{\Tbase}{T_{\text{base}}}
\newcommand{\nc}{n_{c}}
\newcommand{\nf}{n_{f}}
\begin{document}

\pagestyle{empty}

%%--Página de Capa--%%
\begin{titlepage}
    
    \begin{figure}
		\centering
		\includegraphics[scale=0.25]{figs/logo.png}
	\end{figure}
    
    \begin{center}
	
	\end{center}
    
    \vspace*{\fill}
    
    \begin{center}
      {\Huge Homework Task for 3db Access AG Technical Interview}\\[2cm]
      {\Huge --- }\\[0.3cm]
      {\Large Design of a PWM Controller and Open Problem}\\[0.5cm]
    \end{center}
    
    \vspace*{\fill}
    
    \begin{center}
		{\Large José Pedro Castro Fonseca}\\[0.4cm]
    \end{center}
    
    \bigskip
    
    \begin{center}
		{\large Zurich, 13th November de 2014}
    \end{center}
    
\end{titlepage}

\newpage

\pagestyle{fancy}

\section{Initial Considerations} \label{section:init_spec}

Before presenting the architecture details in Section \ref{section:ef_async_param}, I make here some general
considerations about the problem, as well as presenting some notation conventions I adopt throughout the text. Lastly, I
present the response to items 6.1.1 and 6.1.2 in Sections \ref{section:fcore_calc} and \ref{section:ef_async_param}.

Foremost, it is pertinent to note that the requested switching frequency range, 

\begin{equation*}
    [2000 \mhz, 1750 \mhz, 1500 \mhz, ...,500 \mhz]
\end{equation*}

can actually be represented as $1/8$th parts of the maximum frequency $\fmax = 2000 \mhz$, since $250 \mhz =
1/8 \fmax$. Therefore, we can conveniently express the user-selectable frequency, $\fselk$, as

\begin{equation}\label{eqn:fsel_def}
    \fselk = \frac{k}{8} \fmax, ~k \in [2,8].
\end{equation}


\subsection{Notation conventions} \label{section:fcore_calc}

\begin{itemize}
    \item $\lcm(a,b)$ is the \textit{Least Common Multiple} of the numbers in the range $a$ to $b$.
    \item $|a|$ is the \textit{Absolute Value} if the number, or expression, $a$.
    \item $\logt(a)$ is the \textit{Logarithm of \textbf{base 2}} of the number $a$, i.e. $\logt(a) =
          \frac{\ln(a)}{\ln(e)}$
\end{itemize}


\section{Problem 1 - PWM Architecture Proposals}

\subsection{Preliminary Questions}
\subsubsection{Calculation of $\fcore$} \label{section:ef_async_param}
The calculation of $\fcore$ will actually depend on which architecture we choose. The frequency requirement of $\fmax$,
for each of the individual architectures I proposed, refer to Sections \label{section:arch1}, \label{section:arch2}, \label{section:arch3}.

\subsubsection{Practical effects of the ASYNC parameter} \label{section:ef_async_param}
It is my undestanding that this parameter represents the synchronization between \verb|core_clk| and \verb|pclk|, that
is, if they \textbf{run at the same frequency}. If this is true, then this would trigger compile-time architectural
simplifications, hence the need for this parameter, so that we can conditionally compile the clock crossing mechanism
between the registers in the APB Domain and the Core Domain. 

In the case of \textbf{asynchronous} clocks (i.e. \verb|ASYNC|=0), we would need
an Asynchronous FIFO, with gray encoded write and read pointers, to perform the data passing between the APB to the PWM
Core, and vice-versa. Here we would queue the new frequency/resolution/polarity value requests, so that the PWM core could
reconfigure itself after each period of the switching frequency. 

In the case of \textbf{synchronous} clocks (i.e. \verb|ASYNC|=1), a simple Synchronous FIFO would suffice.

Since it is said that \verb|core_clock| is either as fast, or faster, than \verb|pclk|, it can happen that the
\verb|core_clk| frequency requirement is very high (due to high frequency/resolution requirements for the PWM Generation), 
and this would unneedingly burden the clock requirements of the APB bus speed. Therefore, this parameter allows us to ``decouple'' the 
possible high frequency requirements of \verb|core_clock| from \verb|pclk|, allowing them to each run at its appopriate speed.

\subsection{Architecture Proposals}\label{section:arch_prop}

\subsection{Architecture 1 - Counter Based}\label{section:arch1}
The PWM generation based on binary counters is the most basic PWM generation architecture, that is seen especially in 
microcontrollers, where you are simply given the option to write to the counter-compare and duty cycle-compare
registers, and where the user needs to calculate the appropriate values that are closest to the desired frequency and duty
cycle. However, here this problem is a bit more complicated, since the allowed frequencies are specified at the start.
The proposed architecture can be seen in Figure. %%TODO \ref{fig:arch1}.

The first $D$-bit counter divides the incoming clock, $\fcore$ into $\fbase$, by means of a comparison with a Period
Word LUT, that chooses the apropriate comparison term according to the frequency $\fselk$ selected. Note that this
$\fbase$ is actually a strobe that enables the 16-bit counter, serving as a \textbf{timebase} for it such that, using
Equation \ref{eqn:fsel_def}, we get

\begin{equation}\label{eqn:fbase_def}
    2^R \frac{1}{\fbase} = \frac{1}{\fselk} \\ \eqv \fbase = 2 \frac{k}{8} \fmax \eqv 2^{16-3} k \fmax = 2^{13} k \fmax
\end{equation}
where $R$ is the chosen resolution for the PWM operation. Whenever we choose a resolution $R < \Rmax$, we do not need to
perform any additional division on $\fcore$, since removing one bit from the counter comparison has the equivalent
effect to also divide by half the running frequency if we only use its 15 most significant bits. The same logic applies
to other $R$s.

In order to calculate the required $\fcore$ that would work for all $(R,k)$ combinations, as we just saw, we simply need
to care about the $k$ combinations. To that end, $\fcore$ needs to be divided into $\fbasek$  by some integer word
$\verb|PW|(k)$, that depends on the desired frequency multiple $k$.  If we define $\fcore$ as being some multiple $N$ of
$\fbase$, such that

\begin{equation}\label{eqn:fcore_def}
    \fcore = N 2^{13} \fmax ~~ \text{and} ~~ \verb|PW|(k) = \left[ \frac{N}{k} \right],
\end{equation}
then the only way \verb|PW(k)| is an integer is if $N$ is the \textbf{Least Common Multiple}
of the set of possible $k$ values. Then $k$ would evenly divide $\lcm(2,8)$, and all $\fbasek$ are
integer multiples of $\fcore$.

If we compute $\lcm(2,8)$ (for instance, using the \verb|numpy| package in python, with the command

\begin{verbatim}
numpy.lcm.reduce(range(2,8+1))
\end{verbatim}
we find that $\lcm(2,8) = 840$. This would yield

\begin{equation}
    \fcore = \lcm(2,8) 2^{13} \fmax = \lcm(2,8) 2^{13} 2 \khz = 13.76256 \ghz,
\end{equation}
which is a non feasible frequency to use in a digital system. No counter or complex logic could
run at such high speed, not to even mention the power consumption of such a system (modern commercial processors run, at
most when overclocked, at around $5 \ghz$). It may seem strange that a PWM generator with a maximum switching frequency
of merely $2\khz$, could have such a high frequency requirement. However, we should not that the resolution is also
quite high, and even if the frequency were fixed at this value, $\fcore = \fbase(8) = 131.072\mhz$, which would be a
feasible frequency, but still very high compared with $\fmax$. To make matters worse, the wide range of frequencies,
which are not even multiples of 2 from each other, greatly elevates the $\lcm$ factor of $\fcore$.

\begin{framed}
    \textbf{Note}: an immediate alternative to this is to bypass the requirement of having a single clock, \\
    or needing to divide it in the module. If we externally multiplex the clocks coming from a PLL (ASIC solution), or instantiate
    a Clock Manager to perform the clock divisions (FPGA solution, MMCM in Xilinx and altpll in Altera). 
    This alternative is discussed in the architecture of Section \ref{section:arch3}.
\end{framed}

Given the infeasibility of $N=\lcm(2,8)$, let us then try to find another, more feasible, value for $N$, and fix $\fbase(k=8) = 2^{16}
\fmax$. This allows us to at least precisely time $\fsel = 2\khz$ at any resolution, but incurr on a small error for
frequencies not multiples of 500 (i.e. $k \in \{3,5,6,7\})$). If we can exactly characterize this error, and choose an
$N$ that bounds it into a predefined acceptable range, this solution can still be of use. According to our definition of 
\verb|PW|(k), it follows that

\begin{equation*}
    \fbasek = \frac{\fcore}{PW(k)} = \frac{N}{PW(k)} 2^{13} \fmax.
\end{equation*}
The minimum time resolution we could then measure with $\fbasek$ is therefore

\begin{equation}\label{eqn:tbasea_def}
   \Tbasea = \frac{1}{\fbase} 2^{16-R} = \frac{PW(k)}{N} \frac{2^{16_R}}{2^{13} \fmax} = \frac{PW(k)}{N}
   \frac{8}{2^{R}\fmax},
\end{equation}
where we already incorporate the effect that, using a given resolution $R$, is the equivalent of halving the frequency
(or doubling the period) by $2^{16-R}$. On the other hand, the theoretical resolution would be equal to dividing
$\fselk$ by $2^{R}$, tha tis

\begin{equation}\label{eqn:tbase_def}
   \Tbase = \frac{\frac{1}{\fbase}}{2^{R}} = \frac{8}{k \fmax 2^{R}} = \frac{1}{k \fmax 2^{R-3}}. 
\end{equation}
Using equations \ref{eqn:tbase_def} and \ref{eqn:tbasea_def}, we can define the error on the minimum time resolution to
be

\begin{equation*}
    E = \left|\frac{\Tbasea}{\Tbase} - 1 \right| = 
        %%\left|\frac{ \frac{ \frac{8PW(k)}{N \fmax 2^{R}} } }{ \frac{1}{k \fmax 2^{R-3}} } -1 \right| 
        \left|\frac{8 k PW(k) \fmax 2}{N\fmax 2^{R}} - 1 \right| , 
\end{equation*}
which, taking into account Equation \ref{eqn:fbase_def}, simply becomes,

\begin{equation}\label{eqn:err_res}
    E = \left|\frac{k}{N} \left[ \frac{N}{k} \right] - 1 \right|,
\end{equation}
which confirms the initial thesis that $N$ must be divisible by $k$ in order for the error to be 0 for all $k$
(otherwise, we have a fractional part, and then $ \left[ \frac{N}{k} \right] \neq  \frac{N}{k}$. Using the python
script located in \verb|pwm_module/scripts/PwmGenModel.py|, we can plot the error for a selection of combinations
of $(N,k)$, obtaining
figure \ref{fig:err_arch1}.

\begin{figure}[h!]
    \centering
    \includegraphics[scale=0.7]{figs/error_pwm_counter.png}
    \caption{Time precision error, as a function of $k$, for several $N$}
    \label{fig:err_arch1}
\end{figure}
As predicted, we see that for the multiples of 500, the error is always zero when $N$ is a power of two. For
$N = 32$ ($\fcore = 524.288 \mhz$), the error stays within 10\%. For $N=64$ ($\fcore = 1.048576 \ghz$), 
the error stays within 5\%. This is still a sub-optimal result, which must make us reformulate the present architecture.

\subsection{Architecture 2 - Delay Line Based}\label{section:arch2}
A way to alleviate the high frequency requirement of the counter based architecture is to delegate the
smallest-resolution time interval measurements to a chain of delay elements (known in literature as a
\textit{delay-line}, whose intrinsic delay is independent of $\fcore$ (but subject to PVT variations up to a certain
point). This architecture is depicted on the diagram of Figure %\ref{fig:arch2}.

Assume that the chosen resolution $R$ is split between $\nc$ \textit{coarse} delay bits, and 
$\nf$ \textit{fine} delay bits, such that $R=\nf+\nc$. The coarse delay is handled by the $\nc$-bit counter at the
top of the picture which, being clocked by $\fbasek$, counts time in $\frac{1}{\fbasek}$ increments, whose ammount is
dictated by the value in register \verb|DUTYC_HI|, a slice of the $\nc$ MSBs of \verb|DUTYC[15:0]|. And because this
counter is clocked by $\fbasek = 2^{\nc} \fselk$, when it wraps around we count $2^{\nc}$ cycles, therefore generating
a compare match pulse at every period of $\fselk$, serving the purpose of our \verb|SET| signal for the switching period
On the other hand, the compare match pulse between the $\nc$-bit counter with \verb|DUTYC_HI| is fed to an array of
parallel delay lines, whose \textbf{total} delay equals one period of $\fbase$. The rationale here is that we use the
fine delay simply to \textit{subdivide} the smallest coarse-delay element we can get, and apply a delay resolution where
the latter fails to achieve. 

Mathematically speaking, the equations for $\fbasek$ and $\fcore$ remain the same, with the exception that now their
``equivalent'' $R$ is $\nc < R$, which reduces the clock frequency requirements by $2^{R-nf}$. We then have that

\begin{equation*}
    \fbasek = 2^{\nc} \fselk ~~ \text{and} ~~ \fcore = \lcm(2,8) 2^{\nc-3} \fmax
\end{equation*}
such that, taking into account the definition of \verb|PW|(k) in Equation \ref{eqn:fcore_def}, and
replacing $N=\lcm(2,8)$ for even division, we get

\begin{equation}
    \frac{\fcore}{PW(k)} = \frac{k}{\lcm(2,8)} \lcm(2,8) k  2^{\nc-3} \fmax = \fbasek
\end{equation}

The reason why I propose using separate delay-lines for each frequency is that, if we used a single one,
we would incur again in the issue of trying to find a elementary delay whose multiples express the elementary
delays for each frequency, which would needlessly increase the multiplexer and \verb|DUTYC_LO| decoder logic.


\subsection{Architecture 3 - Counter Based, with clock manager}\label{section:arch3}

\subsection{Summary and comparison between architectures}\label{section:arch_summary}

\pagebreak
\section{Problem 2 - Open Problem November 2020}
First of all, I think that the functionality of each module should be better explained, since the diagram, as it is,
requires a lot of guesswork to be understood. It is okay, but of course the quality of my response will degrade, when 
comparing with a scenario with more information/documentation. But I understand that the goal might be to evaluate the
deduction capabilities of the candidate when presented with a limited-information scenario.

\subsection{Possible Causes}
Since no analog blocks have changed, the problem must reside either in the changes made to the Digital Baseband block,
or something about the \textbf{interface} between the Digital Baseband and the Analog frontend. On the other hand, since 
the degradation occurs already at the BER metric, this can indicate a problem on the module that feeds both the 
\verb|data demodulation| (where I assume the original bit stream is recovered, and the BER is calculated) and the \verb|TOA|
(Time-of-Arrival, I presume?). Another possibility is that the \verb|QC| (I assume is the Quadrature Corrector, and also
possibly the I-Q demodulator? I can't know, there is no sufficent information) is actually okay, and the problem is in 
the \verb|TOA|, since it seems like the data \verb|data demodulation| block also depends on the \verb|TOA|'s output.
It can be even that the problem is in the transmitter part of the circuit, since it may be adding some sort of imbalance
to the phase signal, that then causes an I-Q phase imbalance which the \verb|QC| of the other chip is not able to
handle. It can additionally be a combination of problems in both the transmitter and the receiver.

\subsection{Proposed Debugging Methodology}

\subsubsection{Step 1: Pair the 5th generation chip with a 4th generation chip}
Assuming that the 4th and 5th generation chips are protocol-compatible (i.e. they can communicate with each other, the
only difference being in the recovery/demodulation/ToA algorithms and configurability), I would start by evaluating 
the communication between a 4th gen. chip and a 5th gen. chip, and check the BER and distance calculated in
both. In terms of setup, I would try to configure the 5th gen. chip to be as much as possible identical to the 4th
gen., and try to choose the most ``neutral'' values for the additional configuration registers the 5th generation chip
has, and that the 4th gen. has not.

\begin{itemize}
    \item If the BER degradation is also observed in the 4th gen. chip, this means that the problem might be already on
    the transmitter of the 5th gen. chip.
    \item If the BER degradation is observed only in the 5th gen. chip, then this means that the problem is really on
    the receiving/demodulation logic of the 5th gen. chip.
    \item If the BER degradation is observed in both gens., then both the transmission, and reception blocks, can have
    problems.
\end{itemize}

After this step, I could have a more educated guess if the Tx or Rx logic is the culprit. With this knowledge in mind, I
could then direct my following efforts more in the direction of the Tx or Rx blocks. Worse-case scenario, to both, if
the 3rd condition I stated above is the one that is observed...

\subsubsection{Step 2: Observation of internal signals/states}
I see that we can route internal signals, which are multiplexed into the 5 GPIO outputs by the \verb|GPIO Matrix|. The
internal signals I would try to observe would be (in order, and supposing we would have access to them):

\begin{enumerate}
    \item \textbf{The current states of the internal FSMs}, to make sure they are running as they should, or that they are
    stuck in some state, etc.. (if the machines are encoded as one-hot, this might be a problem, because 5 bits 
    is too little... it is still possible, but we might need to incrementally check the all the bits).

    \item \textbf{The transmitter signals}, and compare with what is observed in the 4th gen. chip (assuming it also has
    internal observability through GPIOs?).

    \item \textbf{The receiver signals}, 
        
    \begin{enumerate}
        \item The I-Q samples before and after demodulation. I would even assemble a system that could capture these two
        sample streams (maybe and FPGA dev board?), dump them into a file, and use them against a (possibly existing?) 
        sofware model (python, MATLAB, ... ?) of the \verb|QC| and \verb|data demodulation| blocks.

        \item The TOA internal signals

        \item The TOA inputs and outputs, and again compare them agains a software module. Or even against a similar
        output in a 4th gen. chip.

    \end{enumerate}

\end{enumerate}

\end{document}
